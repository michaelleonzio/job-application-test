<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    //
    protected $fillable = [
        'firstName','lastName'
    ];
    public function classes()
    {
        return $this->hasNoneToMany('App\Classes','id');
    }
}
