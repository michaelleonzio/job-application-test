<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    //
    protected $fillable = [
        'name'
    ];
    public function teacher()
    {
        return $this->belongsTo('App\Teacher','teacher_id');
    }
    public function student()
    {
        return $this->hasMany('App\Student','class_id');
    
    }
}
