<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //
    protected $fillable = [
        'firstName','lastName'
    ];
    public function classes()
    {
        return $this->belongsTo('App\Classes','id');    
    }
   
}
