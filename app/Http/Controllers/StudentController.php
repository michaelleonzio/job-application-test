<?php

namespace App\Http\Controllers;

use App\Classes;
use App\Student;
use App\Http\Requests\AddFormStudent;
use Cache;
use Auth;

class StudentController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function addForm()
    {
        return view('AddStudentForm');
    }
    public function addStudent(AddFormStudent $addFormStudent)
    {
        $student = new Student;
        $student->firstName=$addFormStudent->input('firstName');
        $student->lastName=$addFormStudent->input('lastName');
        if ($student->save()) {
            Session()->flash('message', 'the new Student has been added');
            return redirect()->route('home');
        } else {
            Session()->flash('message', 'Add Student failed');
            return redirect()->route('home');
        }
    }
    public function deleteForm()
    {
        $student = Student::get();
        $class = Classes::get();
        if (count($student)>0) {
            return view('DeleteStudentFormSelectClassForm', compact('class'));
        } elseif (count($student)<1) {
            Session()->flash('message', 'there is no student to delete');
            return redirect()->route('home');
        }
    }
    public function deleteFormNext()
    {
        $class_id = request('class');
        if ($class_id === 'all') {
            $class_name='All';
            $student = Student::get();
            return view('DeleteStudentFormSelectStudentForm', compact('student', 'class_name'));
        } else {
            $student=Student::where(['class_id'=>$class_id])->get();
            $class=Classes::where(['id'=>$class_id])->first();  
            $class_name=$class->name;
            return view('DeleteStudentFormSelectStudentForm', compact('class', 'class_name'));
        }
    }
    public function deleteStudent()
    {
        $student=request('student');
        Student::where(['id'=>$student])->delete();
        Session()->flash('message', 'the selected Student has been deleted');
        return redirect()->route('home');
    }
    public function assignStudent()
    {
        $student = Student::where(['class_id'=>NULL])->get();
        if (count($student)>0) {
            $class = Classes::get();
            if (count($class)>0) {
                return view('AssignStudentSelectStudentForm', compact('student'));
            } elseif (count($class)<1) {
                Session()->flash('message', 'no class available'
                        . ', please add a class before you assign a student');
                return redirect()->route('home');
            }
        } elseif (count($student)<1) {
            Session()->flash('message', 'there is no student to assign');
            return redirect()->route('home');
        }
    }
    public function assignStudentNext()
    {
        $student_id = request('student');
        $student = Student::where(['id'=>$student_id])->first();
        $class = Classes::get();
        return view('AssignStudentSelectClassForm', compact('class', 'student'));
    }
    public function assignStudentProcess()
    {
        $student_id = request('student_id');
        $class_id = request('class');
        if (Student::where(['id'=>$student_id])->update(['class_id'=>$class_id])) {
            Session()->flash('message', 'the selected Student has been assign');
            return redirect()->route('home');
        } else {
            Session()->flash('message', 'assign student failed');
            return redirect()->route('home');
        }
    }
    public function studentData()
    {
        $student = Student::get();
        if (count($student)>0) {
            return view('StudentsData', compact('student'));
        } elseif (count($student)<1) {
            Session()->flash('message', 'there is no student to view');
            return redirect()->route('home');
        }
    }
    public function editDataForm()
    {
        $student = Student::get();
        if (count($student)>0) {
            return view('EditStudentDataForm', compact('student'));
        } elseif (count($student)<1) {
            Session()->flash('message', 'there is no student to edit');
            return redirect()->route('home');
        }
    }
    public function editDataFormNext()
    {
        $student_id = request('student_id');
        $student = Student::where(['id'=>$student_id])->first();
        Cache::put(Auth::user()->id, $student, 100);
        if ( $student ) {
            return view('EditStudentDataFormNext', compact('student'));
        } else {
            return redirect()->back();
        }
    }
    public function editData(AddFormStudent $addFormStudent)
    {
        $first_name = $addFormStudent->input('firstName');
        $last_name = $addFormStudent->input('lastName');
        $student=Cache::get(Auth::user()->id);
        if (Student::where(['id'=>$student->id])->update(['firstName'=>$first_name,'lastName'=>$last_name])) {
            Session()->flash('message', 'the student data has been successfully changed ');
            return redirect()->route('home');
        } else {
            Session()->flash('message', 'failed to change student data ');
            return redirect()->route('home');
        }
    }
    public function moveForm()
    {
        $student = Student::get();
        if (count($student)>0) {
            return view('MoveStudentForm', compact('student'));
        } elseif (count($student)<1) {
            Session()->flash('message', 'there is no student to move ');
            return redirect()->route('home');
        }
    }
    public function moveFormNext()
    {
        $student_id = request('student_id');
        $student = Student::where(['id'=>$student_id])->first();
        $student_class = Classes::find($student->class_id);
        $class = Classes::get();
        Cache::put(Auth::user()->id, $student, 100);
        if ($student) {
            return view('MoveStudentFormNext', compact('student', 'class','student_class'));
        } else {
            return redirect()->back();
        }
    }
    public function move()
    {
        $class_id = request('class_id');
        $student=Cache::get(Auth::user()->id);
        if (Student::where(['id'=>$student->id])->update(['class_id'=>$class_id])) {
            Session()->flash('message', 'the student class has been successfully changed ');
            return redirect()->route('home');
        } else {
            Session()->flash('message', 'failed to change a student class ');
            return redirect()->route('home');
        }
    }
    public function personal($student)
    {
        $students = Student::find($student);
        $class = Classes::find($students->class_id);
        //Cache::put(Auth::user()->id, $class, 100);
        if ($class) {
            return view('StudentPersonalData', compact('students','class'));
        } else {
            Session()->flash('message', 'the student doesnt has any class yet ');
            return redirect()->route('home');
        }
    }
    
}
