<?php

namespace App\Http\Controllers;

use App\Classes;
use App\Teacher;
use App\Http\Requests\AddFormTeacher;
use Cache;
use Auth;

class TeacherController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function addForm()
    {
        return view('AddTeacherForm');
    }
    public function addTeacher(AddFormTeacher $addFormTeacher)
    {
        $teacher = new Teacher;
        $teacher->firstName=$addFormTeacher->input('firstName');
        $teacher->lastName=$addFormTeacher->input('lastName');
        if ($teacher->save()) {
            Session()->flash('message', 'the new Teacher has been added');
            return redirect()->route('home');
        } else {
            Session()->flash('message', 'Add new Teacher failed');
            return redirect()->route('home');
        }
    }
    public function deleteForm()
    {
        $teacher = Teacher::get();
        if (count($teacher)>0) {
            return view('DeleteTeacherForm', compact('teacher'));
        } elseif (count($teacher)<1) {
            Session()->flash('message', 'there is no teacher to delete');
            return redirect()->route('home');
        }
    }
    public function deleteTeacher()
    {
        $teacher_id=request('teacher');
        $teach=Teacher::where(['id'=>$teacher_id])->first();
        $class=Classes::where(['teacher_id'=>$teacher_id])->get();
        if (count($class)<1) {
            $teach->delete();
            Session()->flash('message', 'the selected teacher has been deleted');
            return redirect()->route('home');
        } elseif (count($class)>0) {
            Session()->flash('message', 'the selected teacher is currently assigned to a class and cannot be deleted');
            return redirect()->route('home');
        }
    }
    public function teacherData()
    {
        $teacher = Teacher::get();
        if (count($teacher)>0) {
            return view('TeachersData', compact('teacher'));
        } elseif (count($teacher)<1) {
            Session()->flash('message', 'there is no teacher to view');
            return redirect()->route('home');
        }
    }
    public function editDataForm()
    {
        $teacher = Teacher::get();
        if (count($teacher)>0) {
            return view('EditTeacherDataForm', compact('teacher'));
        } elseif (count($teacher)<1) {
            Session()->flash('message', 'there is no teacher to edit');
            return redirect()->route('home');
        }
    }
    public function editDataFormNext()
    {
        $teacher_id = request('teacher_id');
        $teacher = Teacher::where(['id'=>$teacher_id])->first();
        Cache::put(Auth::user()->id, $teacher, 100);
        if ($teacher) {
            return view('EditTeacherDataFormNext', compact('teacher'));
        } else {
            return redirect()->back();
        }
    }
    public function editData(AddFormTeacher $addFormTeacher)
    {
        $first_name = $addFormTeacher->input('firstName');
        $last_name = $addFormTeacher->input('lastName');
        $teacher=Cache::get(Auth::user()->id);
        if (Teacher::where(['id'=>$teacher->id])->update(['firstName'=>$first_name, 'lastName'=>$last_name])) {
            Session()->flash('message', 'the teacher data has been successfully changed ');
            return redirect()->route('home');
        } else {
            Session()->flash('message', 'failed to change Teacher data ');
            return redirect()->route('home');
        }
    }
    public function personal($teacher)
    {
        $teachers = Teacher::find($teacher);
        $class=Classes::where(['teacher_id'=>$teacher])->get();
        //Cache::put(Auth::user()->id, $class, 100);
        if ($teacher) {
            return view('TeacherPersonalData', compact('teachers','class'));
        } else {
            return redirect()->back();
        }
    }
}
