<?php

namespace App\Http\Controllers;

use App\Classes;
use App\Teacher;
use App\Student;
use App\Http\Requests\AddFormClass;
use Cache;
use PDF;
use Auth;

class ClassController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function addForm()
    {
        $teacher = Teacher::get();
        if (count($teacher)>0) {
            return view('AddClassForm',compact('teacher'));
        } elseif (count($teacher)<1){
            Session()->flash('message', 'no teacher available to manage a class'
                    . ', please add a teacher before you add a class');
            return redirect()->route('home');
        }
    }
    public function addClass(AddFormClass $addFormClass)
    {
        $class = new Classes;
        $class->name=$addFormClass->input('name');
        $class->teacher_id=request('teacher');
        if ($class->save()) {
            Session()->flash('message', 'the new class has been added');
            return redirect()->route('home');
        } else {
            Session()->flash('message', 'Add Class failed');
            return redirect()->route('home');
        }
    }
    public function deleteForm()
    {
        $class = Classes::get();
        if (count($class)>0) {
            return view('DeleteClassForm', compact('class'));
        } elseif (count($class)<1) {
            Session()->flash('message', 'there is no class to delete');
            return redirect()->route('home');
        }
    }
    public function deleteClass()
    {
        $class_id=request('class');
        $classes=Classes::with('Student')->find($class_id);
        if (count($classes->Student)<1) {
            $classes->delete();
            Session()->flash('message', 'the selected class has been deleted');
            return redirect()->route('home');
        } elseif (count($classes->Student)>0) {
            return view('DeleteClassAndStudentsForm', compact('classes'));
        }
    }
    public function deleteClassAndStudent()
    {
        $class_id=request('class_id');
        Student::where(['class_id'=>$class_id])->delete();
        $student = Student::where(['class_id'=>$class_id])->get();
        if (count($student)===0) {
            Classes::where(['id'=>$class_id])->delete();
            Session()->flash('message', 'the selected Class and its students have been deleted');
            return redirect()->route('home');
        } else {
            Session()->flash('message', 'failed to delete the class and its students');
            return redirect()->route('home');
        }
    }   
    public function changeTeacherForm()
    {
        $class = Classes::get();
        if (count($class)>0) {
            return view('UpdateClassTeacherSelectClassForm', compact('class'));
        } elseif (count($class)<1) {
            Session()->flash('message', 'there is no class to assign new teacher');
            return redirect()->route('home');
        }
    }
    public function changeTeacherFormNext()
    {
        $class_id=request('class');
        $class=Classes::where(['id'=>$class_id])->first();
        $teacher=Teacher::where(['id'=>$class->teacher_id])->first();
        $teachers=Teacher::get();
        if ($class_id) {
            return view('UpdateClassTeacherSelectTeacherForm', compact('class', 'teacher', 'teachers'));
        } else {
            return redirect()->back();
        }
    }
    public function changeTeacher()
    {
        $teacher_id=request('teacher');
        $class_id=request('class');
        $class=Classes::where(['id'=>$class_id])->first();
        if (Classes::where(['id'=>$class_id])->update(['teacher_id'=>$teacher_id])) {
            Session()->flash('message', 'the assigned teacher for has been successfully changed ');
            return redirect()->route('home');
        } else {
            Session()->flash('message', 'failed to change the assign teacher ');
            return redirect()->route('home');
        }
    }
    public function classesData()
    {
        $class = Classes::with('Teacher', 'Student')->get();
        if (count($class)>0) {
            Cache::put(Auth::user()->id, $class, 100);
            return view('ClassesData', compact('class'));
        } elseif (count($class)<1) {
            Session()->flash('message', 'there is no class to download or view');
            return redirect()->route('home');
        }
    }
    public function downloadPDF()
    {
        $class=Cache::get(Auth::user()->id);
      //  $class=Classes::with('Teacher','Student')->find($value);
        $pdf = PDF::loadView('ClassesDataPdf', compact('class'));
        if ($pdf) {
            return $pdf->download('classDataPdf.pdf');
        } else {
            Session()->flash('message', 'download failed');
            return redirect()->back('home');
        }
    }
    public function classData()
    {
        $class = Classes::with('Student')->get();
        if (count($class)>0) {
            return view('ClassData', compact('class'));
        } elseif (count($class)<1) {
            Session()->flash('message', 'there is no class to view');
            return redirect()->route('home');
        }
    }
    public function editDataForm()
    {
        $class = Classes::get();
        if (count($class)>0) {
            return view('EditClassDataForm', compact('class'));
        } elseif (count($class)<1) {
            Session()->flash('message', 'there is no class to edit');
            return redirect()->route('home');
        }
    }
    public function editDataFormNext()
    {
        $class_id = request('class_id');
        $class = Classes::where(['id'=>$class_id])->first();
        Cache::put(Auth::user()->id, $class, 100);
        return view('EditClassDataFormNext', compact('class'));
    }
    public function editData(AddFormClass $addFormClass)
    {
        $name = $addFormClass->input('name');
        $class=Cache::get(Auth::user()->id);
        if (Classes::where(['id'=>$class->id])->update(['name'=>$name])) {
            Session()->flash('message', 'the class data has been successfully changed ');
            return redirect()->route('home');
        } else {
            Session()->flash('message', 'failed to change class data ');
            return redirect()->route('home');
        }
    }
    public function search($class)
    {
        $classes = Classes::with('Teacher', 'Student')->find($class);	
        //Cache::put(Auth::user()->id, $class, 100);
        if ($classes) {
            return view('class2Data', compact('classes','class'));
        } else {
            return redirect()->back();
        }
    }
    public function classPDF($class)
    {
        //$value=Cache::get(Auth::user()->id);
        $classes=Classes::with('Teacher', 'Student')->find($class);
        $pdf = PDF::loadView('Class2DataPDF', compact('classes'));
        if ($pdf) {
            return $pdf->download('Class2DataPDF.pdf');
        } else {
            Session()->flash('message', 'download failed');
            return redirect()->back('home');
        }
    }
}
