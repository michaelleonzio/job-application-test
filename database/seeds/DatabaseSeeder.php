<?php

use Illuminate\Database\Seeder;
use App\Teacher;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $faker = Faker::create();
        for ($i=0;$i<10;$i++) {
	        Teacher::create([
	            'firstName' => $faker->firstName,
	            'lastName' => $faker->lastName,
	            
	        ]);
    	 }
    }
}
