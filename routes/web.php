<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/Class/Add/Form','ClassController@addForm')
        ->name('addClassForm');
Route::post('/Class/Add/Process','ClassController@addClass')
        ->name('addClass');
Route::get('/Teacher/Add/Form','TeacherController@addForm')
        ->name('addTeacherForm');
Route::post('/Teacher/Add/Process','TeacherController@addTeacher')
        ->name('addTeacher');
Route::get('/Student/Add/Form','StudentController@addForm')
        ->name('addStudentForm');
Route::post('/Student/Add/Process','StudentController@addStudent')
        ->name('addStudent');
Route::get('/Class/Delete/Form','ClassController@deleteForm')
        ->name('deleteClassForm');
Route::post('/Class/Delete/Process','ClassController@deleteClass')
        ->name('deleteClass');
Route::post('/Class/Delete/All/Student','ClassController@deleteClassAndStudent')
        ->name('deleteClassAndStudents');
Route::get('/Teacher/Delete/Form','TeacherController@deleteForm')
        ->name('deleteTeacherForm');
Route::post('/Teacher/Delete/Process','TeacherController@deleteTeacher')
        ->name('deleteTeacher');
Route::get('/Student/Delete/Form/Select/Class','StudentController@deleteForm')
        ->name('deleteStudentForm');
Route::post('/Student/Delete/Form','StudentController@deleteFormNext')
        ->name('deleteStudentFormNext');
Route::post('/Student/Delete/Process','StudentController@deleteStudent')
        ->name('deleteStudent');
Route::get('/Class/Change/Teacher/Form/Select/Class','ClassController@changeTeacherForm')
        ->name('changeTeacherForm');
Route::post('/Class/Change/Teacher/Form/Select/Teacher','ClassController@changeTeacherFormNext')
        ->name('changeTeacherFormNext');
Route::post('/Class/Change/Teacher/Process','ClassController@changeTeacher')
        ->name('changeTeacher');
Route::get('/Student/Assign/Select/Student','StudentController@assignStudent')
        ->name('assignStudentForm');
Route::post('/Student/Assign/Select/Class','StudentController@assignStudentNext')
        ->name('assignStudentFormNext');
Route::post('/Student/Assign/Process','StudentController@assignStudentProcess')
        ->name('assignStudent');
Route::get('/Class/Data/View','ClassController@classesData')
        ->name('viewClassesData');
Route::get('/Classes/Data/Download/PDF','ClassController@downloadPDF')
        ->name('downloadPDF');
Route::get('/Teacher/Data/View','TeacherController@teacherData')
        ->name('viewTeachersData');
Route::get('/Student/Data/View','StudentController@studentData')
        ->name('viewStudentsData');
Route::get('/Classes/Data/View','ClassController@classData')
        ->name('viewClassData');
Route::get('/Student/Data/Edit/Select/Student','StudentController@editDataForm')
        ->name('editStudentForm');
Route::get('/Teacher/Data/Edit/Select/Teacher','TeacherController@editDataForm')
        ->name('editTeacherForm');
Route::get('/Class/Data/Edit/Select/Class','ClassController@editDataForm')
        ->name('editClassForm');
Route::post('/Student/Data/Edit/Input','StudentController@editDataFormNext')
        ->name('editStudentFormNext');
Route::post('/Teachers/Data/Edit/Input','TeacherController@editDataFormNext')
        ->name('editTeacherFormNext');
Route::post('/Class/Data/Edit/Input','ClassController@editDataFormNext')
        ->name('editClassFormNext');
Route::post('/Student/Data/Edit/Process','StudentController@editData')
        ->name('editStudent');
Route::post('/Teacher/Data/Edit/Process','TeacherController@editData')
        ->name('editTeacher');
Route::post('/Class/Data/Edit/Process','ClassController@editData')
        ->name('editClass');
Route::get('/Student/Move/Class/Select/Student','StudentController@moveForm')
        ->name('moveStudentForm');
Route::post('/Student/Move/Class/Select/Class','StudentController@moveFormNext')
        ->name('moveStudentFormNext');
Route::post('/Student/Move/Class/Process','StudentController@move')
        ->name('moveStudent');
Route::get('/Class/{name}', 'ClassController@search')
        ->name('/Class/{name}');
Route::get('/Class/Data/Download/PDF/{name}','ClassController@classPDF')
        ->name('/Class/Data/Download/PDF/{name}');
Route::get('/Teacher/Data/{name}','TeacherController@personal')
        ->name('/Teacher/Data/{name}');
Route::get('/Student/Data/{name}','StudentController@personal')
        ->name('/Student/Data/{name}');