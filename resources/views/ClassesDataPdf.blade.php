<!DOCTYPE html>

     
               
<html>
	
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
	<title>Class - PDF</title>
         <div class="card-header">View All Class Data</div>

                <div class="card-body">
</head>
<body>
    <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                

                <div class="card-body">
    @foreach($class as $classes)
    
    <label><b>Class</b>&nbsp;:&nbsp;<font style="text-transform: uppercase;">{{$classes->name}}</form></label><br/>
    <label><b>Teacher in charge</b>&nbsp;:&nbsp;<font style="text-transform: capitalize;">{{$classes->Teacher->firstName}}&nbsp;
        {{$classes->Teacher->lastName}}</form></label><br/>
    <label><b>Student in Class</b>&nbsp;:&nbsp;{{count($classes->Student)}}</label><br/>
    <label><b>Students list</b>&nbsp;:</label><br/>
    <table class="table table-bordered">
     
            <thead>
                <tr>
                <th>Id</th><th>First Name</th><th>Last Name</th><br/>
                 </tr>
            
            </thead>
            <tbody>
                        @foreach($classes->Student as $students)
                        <tr>
                            <td>{{$students->id}}</td>
                            <td><font style="text-transform: capitalize;">{{$students->firstName}}</font></td>
                            <td><font style="text-transform: capitalize;">{{$students->lastName}}</font></td>
                        </tr>
                        @endforeach
              
            </tbody>
            </table>
               <br/><br/><br/><br/>
               
    @endforeach
    
</body>
</html>