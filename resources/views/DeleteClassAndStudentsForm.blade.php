@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Delete Class</div>
 
                <div class="card-body">
                
                    
                    <label> <font style="text-transform: uppercase;">{{$classes->name}}</font> class is not empty or at least
                        has a student available&nbsp;</label><br/>
                    <label>the student available in this class &nbsp;:<br/>
                   
                        @foreach($classes->Student as $student)

                        <label> {{$student->id}}&nbsp;<font style="text-transform: capitalize;">{{$student->firstName}}&nbsp;
                            {{$student->lastName}}</font></label><br/>



                        @endforeach

                    <form class="form-horizontal" method="POST" action="{{route('deleteClassAndStudents')}}"> 
                    {{csrf_field()}}
                    <input type="hidden" name="class_id" value="{{$classes->id}}">
                    <label>do you wish to delete the class alongside all the student in the class?</label>
                    <input type="submit" value="delete the class with the students">
                    </form><br/>
                    
                    <a href="{{route('home')}}">return home</a>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

