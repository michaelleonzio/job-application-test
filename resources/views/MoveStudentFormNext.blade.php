@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Change Student Class</div>

                <div class="card-body">
                <form class="form-horizontal" method="POST" action="{{route('moveStudent')}}"> 
                    {{csrf_field()}}
                    <label>
                        <font style="text-transform: capitalize;">
                            {{$student->firstName}}&nbsp;{{$student->lastName}}
                        </font>
                            &nbsp;current class :
                        <font style="text-transform: uppercase;">
                            {{$student_class->name}}
                        </font></label><br/>
                        <label>Choose a class to move <font style="text-transform: capitalize;">{{$student->firstName}}&nbsp;{{$student->lastName}}</font>&nbsp;</label>
                    <select value="class_id" name="class_id" class="form-control">
                        @foreach($class as $classes)

                        <option value="{{$classes->id}}" id="{{$classes->id}}" font style="text-transform: uppercase;">
                            {{$classes->name}}</option>

                        @endforeach

                    </select><br/>
                    <input type="submit" value="choose">
                </form>
                    

                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

