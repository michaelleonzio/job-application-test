@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">edit Teacher</div>

                <div class="card-body">
                    <label><b>Teacher Name</b>&nbsp;:&nbsp; <font style="text-transform: capitalize;">
                        {{$teacher->firstName}}&nbsp;
                        {{$teacher->lastName}}</font></label>
                <form class="form-horizontal" method="POST" action="{{route('editTeacher')}}"> 
                    {{csrf_field()}}
                    
                    <label>Input new Name for the Teacher&nbsp;:</label>
                    <input type="text" name="firstName" font style="text-transform: capitalize;">&nbsp;
                    <input type="text" name="lastName" font style="text-transform: capitalize;"><br/>
                    <input type="submit" value="Edit">
                </form>
                    

                   
                </div>
</div>
@endsection

