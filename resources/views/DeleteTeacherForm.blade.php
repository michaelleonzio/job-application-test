@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Delete Teacher</div>

                <div class="card-body">
                <form class="form-horizontal" method="POST" action="{{route('deleteTeacher')}}"> 
                    {{csrf_field()}}
                    
                    <label>Choose a teacher to delete&nbsp;</label>
                    <select value="teacher" name="teacher" class="form-control">
                        @foreach($teacher as $teacher)

                        <option value="{{$teacher->id}}" id="{{$teacher->id}}" font style="text-transform: capitalize;">
                                {{$teacher->firstName}}&nbsp;{{$teacher->lastName}}
                        </option>

                        @endforeach

                    </select><br/>
                    <input type="submit" value="choose">
                </form>
                    

                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

