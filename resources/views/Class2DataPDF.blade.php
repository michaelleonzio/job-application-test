<!DOCTYPE html>

     
<html>
	
<head>
 <meta charset="utf-8">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                <div class="card-header"><font style="text-transform: uppercase;">{{$classes->name}}</font> Class Data<br/>
                
                
                
                
                <div class="card-body">
	
</head>
<body>
    <label>class&nbsp;:&nbsp;<font style="text-transform: uppercase;">{{$classes->name}}</font></label><br/>
             <label>teacher in charge&nbsp;:&nbsp;
                 <font style="text-transform: capitalize;">
                 {{$classes->Teacher->firstName}}&nbsp;
                 {{$classes->Teacher->lastName}}
                 </font></label><br/>
             <label>students list&nbsp;:</label><br/>
    <table class="table table-bordered">
     
            <thead>
                <tr>
                <th>Id</th><th>First Name</th><th>Last Name</th><br/>
                 </tr>
            
            </thead>
            <tbody>
            
                
                        @foreach($classes->Student as $students)
                        <tr>
                            <td><font style="text-transform: capitalize;">{{$students->id}}</font></td>
                            <td><font style="text-transform: capitalize;">{{$students->firstName}}</font></td>
                            <td><font style="text-transform: capitalize;">{{$students->lastName}}</font></td>
                        </tr>
                        @endforeach
                    
                
            </tbody>
            </table>
                
               

</body>
</html>