<!DOCTYPE html>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                <div class="card-header">View All Students List<br/>
                
                
                <div class="card-body">
<html>
	
<head>

	<title>Students Data</title>
</head>
<body>
    <table class="table table-bordered">
        <th>Student Id</th> <th>Student Name</th> <th>Class Id</th>
    @foreach($student as $students)
    <tr>
        <td>
            <a href="{{ url('Student/Data/'.$students->id) }}" 
                       id="{{$students->id}}" name="{{$students->id}}">
                        {{$students->id}}
            </a>
        </td>
        <td>
            <a href="{{ url('Student/Data/'.$students->id) }}" 
                       id="{{$students->id}}" name="{{$students->id}}"
                       font style="text-transform: capitalize;">
                        {{$students->firstName}}&nbsp;{{$students->lastName}}
            </a>
        </td>
        <td>
            <a href="{{ url('Class/'.$students->class_id) }}" 
                       id="{{$students->class_id}}" name="{{$students->class_id}}">
                        {{$students->class_id}}
            </a>
        </td>
    </tr>
              
               
    @endforeach
                </div></div></div></div></div></div>
    @endsection
</body>
</html>