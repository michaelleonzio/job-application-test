<!DOCTYPE html>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                <div class="card-header">
                    <font style="text-transform: uppercase;">
                    {{$classes->name}}</font> Class Data<br/>
                <a href="{{url('Class/Data/Download/PDF/'.$class)}}" 
                   id="{{$class}}" name="{{$class}}"  target="_blank">
                    Download this class data in PDF
                </a><br/>
                </div>
                
                
                
                <div class="card-body">
     
<html>
	
<head>

	
</head>
<body>
             <label>class&nbsp;:&nbsp;
                 <font style="text-transform: uppercase;">
                    {{$classes->name}}
                 </font>
             </label><br/>
             <label>teacher in charge&nbsp;:&nbsp;
                 <a href="{{ url('Teacher/Data/'.$classes->teacher_id) }}" 
                       id="{{$classes->teacher_id}}" name="{{$classes->teacher_id}}" font style="text-transform: Capitalize;">
                        {{$classes->Teacher->firstName}}&nbsp;
                        {{$classes->Teacher->lastName}}</label><br/>
                 </a>
             <label>students list&nbsp;:</label><br/>
    <table class="table table-bordered">
     
            <thead>
                <tr>
                <th>Id</th><th>First Name</th><th>Last Name</th><br/>
                 </tr>
            
            </thead>
            <tbody>
            
                
                        @foreach($classes->Student as $students)
                        <tr>
                            <td>
                                <a href="{{ url('Student/Data/'.$students->id) }}" 
                                 id="{{$students->id}}" name="{{$students->id}}">
                                        {{$students->id}}
                                </a>
                            </td>
                            <td>
                                <a href="{{ url('Student/Data/'.$students->id) }}" 
                                id="{{$students->id}}" name="{{$students->id}}"
                                font style="text-transform: capitalize;">
                                        {{$students->firstName}}
                                <a/>
                            </td>
                            <td>
                                <a href="{{ url('Student/Data/'.$students->id) }}" 
                                id="{{$students->id}}" name="{{$students->id}}"
                                font style="text-transform: capitalize;">
                                         {{$students->lastName}}
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    
                
            </tbody>
            </table>
                
               @endsection

</body>
</html>