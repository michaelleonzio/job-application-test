@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Teacher</div>

                <div class="card-body">
                <form class="form-horizontal" method="POST" action="{{route('editTeacherFormNext')}}"> 
                    {{csrf_field()}}
                   
                    <label>Choose a Teacher you want to change&nbsp;</label>
                    <select value="teacher_id" name="teacher_id" class="form-control">
                        @foreach($teacher as $teachers)

                        <option value="{{$teachers->id}}" id="{{$teachers->id}}" font style="text-transform: capitalize;">
                           {{$teachers->firstName}} &nbsp;{{$teachers->lastName}}
                        </option>

                        
                        
                        @endforeach

                    </select><br/>
                    
                    <input type="submit" value="edit">
                </form>
                

                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

