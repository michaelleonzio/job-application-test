@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">My School</div>

                <div class="card-body">
                    
                    <div class="panel-body">
                    @if (Session::has('message'))
                            <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    </div>

                    
                    To add, edit, or delete data please drop down the 'Feature' tray on top right.
                    <br/>
                    <a href="{{route('viewClassesData')}}">View all classes data and Download</a><br/>
                
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
