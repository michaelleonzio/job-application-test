@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Class</div>

                <div class="card-body">
                <form class="form-horizontal" method="POST" action="{{route('editClassFormNext')}}"> 
                    {{csrf_field()}}
                   
                    <label>Choose a class you want to change&nbsp;</label>
                    <select value="class_id" name="class_id" class="form-control">
                        @foreach($class as $class)

                        <option value="{{$class->id}}" id="{{$class->id}}" font style="text-transform: uppercase;">
                            {{$class->name}}
                        </option>

                        
                        
                        @endforeach

                    </select><br/>
                    
                    <input type="submit" value="edit">
                </form>
                

                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

