@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Change Teacher</div>

                <div class="card-body">
                    <label>Class Id:&nbsp;{{$class->id}}</label><br/>
                    <label>Class name:&nbsp;<font style="text-transform: uppercase;">
                        {{$class->name}}</font></label><br/>
                    <label>assigned teacher:&nbsp;<font style="text-transform: capitalize;">
                        {{$teacher->firstName}}&nbsp;
                        {{$teacher->lastName}}</font></label><br/>
                        
                <form class="form-horizontal" method="POST" action="{{route('changeTeacher')}}"> 
                    {{csrf_field()}}
                    <input type="hidden" name="class" value="{{$class->id}}">
                    <label>Choose a new teacher for this <font style="text-transform: uppercase;">{{$class->name}}</font> class&nbsp;:</label>
                    <select value="teacher" name="teacher" class="form-control">
                        @foreach($teachers as $teachers)

                            <option value="{{$teachers->id}}" id="{{$teachers->id}}"
                                    font style="text-transform: capitalize;">
                                {{$teachers->firstName}}&nbsp; {{$teachers->lastName}}</option>
                        @endforeach

                    </select><br/>
                    <input type="submit" value="choose">
                </form>
                    

                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

