@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Delete Student</div>

                <div class="card-body">
                <form class="form-horizontal" method="POST" action="{{route('deleteStudent')}}"> 
                    {{csrf_field()}}
                   
                    <label>Choose a student from <font style="text-transform: uppercase;">{{$class_name}}</font> Class to be deleted&nbsp;</label>
                    <select value="student" name="student" class="form-control">
                        @foreach($student as $student)

                        <option value="{{$student->id}}" id="{{$student->id}}" font style="text-transform: capitalize;">
                            {{$student->firstName}}&nbsp;{{$student->lastName}}
                        </option>

                        

                        @endforeach

                    </select><br/>
                    <input type="submit" value="delete">
                </form>
                    

                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

