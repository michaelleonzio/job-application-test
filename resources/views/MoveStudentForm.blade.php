@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Change Student Class</div>

                <div class="card-body">
                <form class="form-horizontal" method="POST" action="{{route('moveStudentFormNext')}}"> 
                    {{csrf_field()}}
                    
                    <label>Choose a student to move&nbsp;</label>
                    <select value="student_id" name="student_id" class="form-control">
                        @foreach($student as $students)

                        <option value="{{$students->id}}" id="{{$students->id}}" font style="text-transform: capitalize">
                           
                            {{$students->firstName}}&nbsp;{{$students->lastName}}
                        </option>

                        @endforeach

                    </select><br/>
                    <input type="submit" value="choose">
                </form>
                    

                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

