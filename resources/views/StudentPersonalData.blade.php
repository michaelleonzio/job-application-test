<!DOCTYPE html>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                <div class="card-header">Student Data<br/>
                
                <div class="card-body">
<html>
	
<head>

	<title>Student Data</title>
</head>
<body>
    
   
    
    <label><b>Student Name</b>&nbsp;:&nbsp;<font style="text-transform: capitalize;">
        {{$students->firstName}}&nbsp;{{$students->lastName}}</font>
                </label><br/>
                <label><b>Class</b>&nbsp;:&nbsp;</label>
                <a href="{{ url('Class/'.$students->class_id) }}" 
                       id="{{$students->class_id}}" name="{{$students->class_id}}"
                       font style="text-transform: uppercase;">
                        {{$class->name}}<br/>
                </a>
                </div></div></div></div></div></div>
    @endsection
</body>
</html>