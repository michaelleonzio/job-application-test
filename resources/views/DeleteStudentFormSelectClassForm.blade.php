@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Delete Student</div>

                <div class="card-body">
                <form class="form-horizontal" method="POST" action="{{route('deleteStudentFormNext')}}"> 
                    {{csrf_field()}}
                    
                    <label>Choose from which class you will delete the student &nbsp;</label>
                    <select value="class" name="class" class="form-control">
                        <option value="all" id="all">All Student</option>
                        @foreach($class as $class)
                            <option value="{{$class->id}}" id="{{$class->id}}" font style="text-transform: uppercase;">
                                {{$class->name}}
                            </option>
                        @endforeach
                        

                    </select><br/>
                    <input type="submit" value="choose">
                </form>
                    

                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

