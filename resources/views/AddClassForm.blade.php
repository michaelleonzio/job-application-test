@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add Class</div>

                <div class="card-body">
                <form class="form-horizontal" method="POST" action="{{route('addClass')}}"> 
                    {{csrf_field()}}
                    <label>input class name&nbsp;:&nbsp;</label>
                    <input type="text" name="name" font style="text-transform: uppercase;" ><br/>
                    <label>Choose a teacher for this class&nbsp;</label>
                    <select value="teacher" name="teacher" class="form-control">
                        @foreach($teacher as $teacher)

                        <option value="{{$teacher->id}}" id="{{$teacher->id}}">{{$teacher->firstName}}&nbsp;{{$teacher->lastName}}</option>

                        
                        
                        @endforeach

                    </select><br/>
                    
                    <input type="submit" value="add">
                </form>
                

                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

