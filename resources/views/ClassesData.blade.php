<!DOCTYPE html>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                <div class="card-header">View All Class Data<br/>
                <a href="{{route('downloadPDF')}}" target="_blank">Download this data in PDF</a><br/></div>
                
                <div class="card-body">
<html>
	
<head>

	<title>Class - PDF</title>
</head>
<body>
    
    @foreach($class as $classes)
    
    <label><b>Class</b>&nbsp;:&nbsp;<font style="text-transform: uppercase;">{{$classes->name}}</font></label><br/>
    <label><b>Teacher in charge</b>&nbsp;:&nbsp;<font style="text-transform: capitalize;">{{$classes->Teacher->firstName}}&nbsp;
        {{$classes->Teacher->lastName}}</form></label><br/>
    <label><b>Student in Class</b>&nbsp;:&nbsp;{{count($classes->Student)}}</label><br/>
    <label><b>Students list</b>&nbsp;:</label><br/>
    <table class="table table-bordered">
     
            <thead>
                <tr>
                <th>Id</th><th>First Name</th><th>Last Name</th><br/>
                 </tr>
            
            </thead>
            <tbody>
                        @foreach($classes->Student as $students)
                        <tr>
                            <td>{{$students->id}}</td>
                            <td><font style="text-transform: capitalize;">{{$students->firstName}}</form></td>
                            <td><font style="text-transform: capitalize;">{{$students->lastName}}</form></td>
                        </tr>
                        @endforeach
              
            </tbody>
            </table>
               <br/><br/><br/><br/>
               
    @endforeach
                </div></div></div></div></div></div>
    @endsection
</body>
</html>