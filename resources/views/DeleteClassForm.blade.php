@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Delete Class</div>

                <div class="card-body">
                <form class="form-horizontal" method="POST" action="{{route('deleteClass')}}"> 
                    {{csrf_field()}}
                    
                    <label>Choose a class to delete&nbsp;:</label>
                    <select value="class" name="class" class="form-control">
                        @foreach($class as $class)

                        <option value="{{$class->id}}" id="{{$class->id}}" font style="text-transform: uppercase;">
                            {{$class->name}}
                        </option>



                        @endforeach

                    </select><br/>
                    <input type="submit" value="choose">
                </form>
                    

                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

