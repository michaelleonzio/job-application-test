@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Assign Student into a Class</div>

                <div class="card-body">
                <form class="form-horizontal" method="POST" action="{{route('assignStudent')}}"> 
                    {{csrf_field()}}
                    
                    <label>Choose a class to assign 
                        <font style="text-transform: capitalize;">
                            {{$student->firstName}} &nbsp;{{$student->lastName}} &nbsp;
                        </font></label>
                    <select value="class" name="class" class="form-control">
                        @foreach($class as $class)
                        
                        <option value="{{$class->id}}" id="{{$class->id}}" font style="text-transform: uppercase;">
                            {{$class->name}}&nbsp;
                        </option>

                        
                        
                        @endforeach

                    </select><br/>
                    <input type="hidden" value="{{$student->id}}" name="student_id">
                    <input type="submit" value="add">
                </form>
                

                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

